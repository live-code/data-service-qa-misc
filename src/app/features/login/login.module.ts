import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { RegistrationComponent } from './pages/registration.component';
import { LostpassComponent } from './pages/lostpass.component';
import { SigninComponent } from './pages/signin.component';
import { HelpComponent } from './pages/help.component';


@NgModule({
  declarations: [LoginComponent, RegistrationComponent, LostpassComponent, SigninComponent, HelpComponent],
  imports: [
    CommonModule,
    LoginRoutingModule
  ]
})
export class LoginModule { }
