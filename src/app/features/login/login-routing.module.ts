import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login.component';
import { SigninComponent } from './pages/signin.component';
import { RegistrationComponent } from './pages/registration.component';
import { LostpassComponent } from './pages/lostpass.component';
import { HelpComponent } from './pages/help.component';

const routes: Routes = [
  {
    path: '', component: LoginComponent,
    children: [
      { path: 'signin', component: SigninComponent },
      { path: 'regi', component: RegistrationComponent },
      { path: 'lostpass', component: LostpassComponent },
      { path: '**', redirectTo: 'signin' },
    ]
  },
  {
    path: 'help', component: HelpComponent,
  },
  { path: '**', redirectTo: '' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
