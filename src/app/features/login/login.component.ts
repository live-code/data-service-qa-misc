import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ds-login',
  template: `
    
    <button routerLink="./signin">sign in</button>
    <button routerLink="../">../</button>
    <button routerLink="signin">sign in</button>
    
    <button routerLink="regi">regi</button>
    <button routerLink="lostpass">pass</button>
    
    <input type="text">
    
    <router-outlet></router-outlet>
    
    <hr>
    <button routerLink="help">Go to help</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
