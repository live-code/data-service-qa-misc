import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DynamicMenuComponent } from './dynamic-menu.component';

const routes: Routes = [{ path: '', component: DynamicMenuComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DynamicMenuRoutingModule { }
