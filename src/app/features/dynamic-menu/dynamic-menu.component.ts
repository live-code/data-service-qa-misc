import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ds-dynamic-menu',
  template: `
    <div class="card">
      <div class="card-header">header</div>
      <div class="card-body">body</div>
    </div>
  `,
  styles: [
  ]
})
export class DynamicMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
