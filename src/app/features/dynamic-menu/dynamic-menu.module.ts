import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicMenuRoutingModule } from './dynamic-menu-routing.module';
import { DynamicMenuComponent } from './dynamic-menu.component';


@NgModule({
  declarations: [DynamicMenuComponent],
  imports: [
    CommonModule,
    DynamicMenuRoutingModule
  ]
})
export class DynamicMenuModule { }
