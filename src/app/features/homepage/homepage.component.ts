import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { config, InitService } from '../../app.module';
import { HelloComponent } from '../../shared/components/hello/hello.component';

@Component({
  selector: 'ds-homepage',
  template: `
    <pre>{{value}}</pre>
    <input type="range" [(ngModel)]="value" min="0" max="50" step="5">
    <h1 [dsPad]="value" margin="10">padding example</h1>
    <h2 [dsColor]="color">Sono colorato - color</h2>
    <h2 [dsBackground]="color" border="1px solid blue">Sono colorato - bg</h2>

    <button (click)="value = 10">10</button>
    <button (click)="value = 20">20</button>
    <button (click)="color = 'cyan'">cyan</button>
    <button (click)="color = 'yellow'">yellow</button>

    <div [dsLoader]="config" *ngFor="let config of configs"></div>
    <button dsUrl="http://www.google.com">go to website</button>

    <div
      dsXhr="https://jsonplaceholder.typicode.com/users/1"
      (loaded)="doSomething($event)"></div>

    <button *dsIfLogged>Admin Button</button>
    <!--<button *dsIfRoleIs="'admin'">Admin Button</button>-->
      
    <div></div>
    <div *ngFor="let name of users; let i = index">
      {{i + 1}} )  {{name}}
    </div>
  `,
  styles: []
})
export class HomepageComponent {
  value = 0;
  color = 'purple';
  users = ['Fab', 'Mario', 'Andrea'];
  configs = [
    { type: HelloComponent, title: 'ciao' },
    { type: HelloComponent, text: 'bla bla' },
    { type: HelloComponent, text: 'bla bla', title: 'completed' },
    { type: HelloComponent },
  ];

  constructor(
    private initService: InitService,
  ) {

    // console.log(initService.data, config)
  }

  doSomething(result): void {
    console.log(result);
  }
}
