import { Directive, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[dsPad]'
})
export class PadDirective {
  @Input() dsPad: number;
  @Input() margin: number;
  @HostBinding('style.fontSize') fontSize = '20px';

  @HostBinding('style.margin') get getMargins(): string {
    return this.margin + 'px';
  }

  @HostBinding('style.padding') get getPadding(): string {
    return this.dsPad * 10 + 'px'
  }


}

