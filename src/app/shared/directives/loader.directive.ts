import {
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Input,
  OnDestroy,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import { HelloComponent } from '../components/hello/hello.component';

@Directive({
  selector: '[dsLoader]'
})
export class LoaderDirective implements OnInit, OnDestroy {
  @Input() dsLoader: any;
  ref: ComponentRef<any>;

  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver
  ) {}

  ngOnInit(): void {
    const factory: ComponentFactory<any> = this.resolver.resolveComponentFactory(this.dsLoader.type);
    this.ref = this.view.createComponent(factory);

    const [type, ...rest] = Object.keys(this.dsLoader)

    for (const key of rest) {
      if (key) {
        this.ref.instance[key] = this.dsLoader[key];
      }
    }
    // this.ref.instance.title = this.dsLoader.title || 'guest'
    // this.ref.instance.text = this.dsLoader.text || ''
  }

  ngOnDestroy(): void {
    this.ref.destroy();
  }
}
