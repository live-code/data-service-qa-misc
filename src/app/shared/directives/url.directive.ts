import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[dsUrl]'
})
export class UrlDirective {
  @Input() dsUrl: string;

  @HostListener('click')
  clickHandler(): void {
    window.open(this.dsUrl)
  }
}
