import { Directive, HostBinding, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { InitService } from '../../app.module';
import { filter, takeUntil, tap } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';

@Directive({
  selector: '[dsIfLogged]'
})
export class IfLoggedDirective implements OnDestroy {
/*  @HostBinding('style.display') get display(): string {
    return this.initService.isLogged ? null : 'none';
  }*/
/*
  @HostBinding() get hidden(): boolean {
    return !this.initService.isLogged;
  }*/
  sub: Subscription;
  subject: Subject<void> = new Subject();

  constructor(
    private initService: InitService,
    private view: ViewContainerRef,
    private template: TemplateRef<any>
  ) {
    this.sub = initService.isLogged$
      .pipe(
        takeUntil(this.subject),
        tap(() => view.clear()),
        filter(isLogged => isLogged)
      )
      .subscribe(() => {
        view.createEmbeddedView(template)
      });
  }

  ngOnDestroy(): void {
    // this.sub.unsubscribe()
    this.subject.next();
  }
}
