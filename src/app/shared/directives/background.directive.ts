import { Directive, ElementRef, Input, OnChanges, Renderer2, SimpleChange, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[dsBackground]'
})
export class BackgroundDirective implements OnChanges {
  @Input() dsBackground: string;
  @Input() border: string;

  ngOnChanges(changes: SimpleChanges): void {
    // console.log(changes)
    if (changes.dsBackground) {
      this.renderer2.setStyle(this.el.nativeElement, 'backgroundColor', this.dsBackground);
    }

    if (changes.border) {
      this.renderer2.setStyle(this.el.nativeElement, 'border', changes.border.currentValue);
    }
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer2: Renderer2
  ) {
  }
}
