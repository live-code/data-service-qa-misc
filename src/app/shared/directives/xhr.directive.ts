import { Directive, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[dsXhr]'
})
export class XhrDirective implements OnDestroy {
  @Output() loaded: EventEmitter<any> = new EventEmitter<any>();
  sub: Subscription;

  @Input() set dsXhr(val) {
    if (this.sub) { this.sub.unsubscribe() }
    this.sub = this.http.get(val)
      .subscribe(res => this.loaded.emit(res))
  }

  constructor(private http: HttpClient) {
  }

  ngOnDestroy(): void {
    console.log('destroy')
    if (this.sub) { this.sub.unsubscribe() }
  }

}
