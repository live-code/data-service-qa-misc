import { Directive, ElementRef, HostBinding, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[dsColor]'
})
export class ColorDirective {
  @Input() set dsColor(val: string) {
    // this.el.nativeElement.style.color = val;
    this.renderer2.setStyle(this.el.nativeElement, 'color', val);
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer2: Renderer2
  ) {
  }

}
