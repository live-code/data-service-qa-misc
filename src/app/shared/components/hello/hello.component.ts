import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ds-hello',
  template: `‘‘
    <p>
      hello {{title}}!
    </p>
    
    <pre>{{text}}</pre>
  `,
  styles: [
  ]
})
export class HelloComponent implements OnInit {
  @Input() title: string;
  @Input() text: string;
  constructor() { }

  ngOnInit(): void {
  }

}
