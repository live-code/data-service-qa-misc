import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PadDirective } from './directives/pad.directive';
import { ColorDirective } from './directives/color.directive';
import { BackgroundDirective } from './directives/background.directive';
import { UrlDirective } from './directives/url.directive';
import { XhrDirective } from './directives/xhr.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { HelloComponent } from './components/hello/hello.component';
import { LoaderDirective } from './directives/loader.directive';



@NgModule({
  declarations: [PadDirective, ColorDirective, BackgroundDirective, UrlDirective, XhrDirective, IfLoggedDirective, HelloComponent, LoaderDirective],
  imports: [
    CommonModule
  ],
  exports: [
    PadDirective,
    ColorDirective,
    BackgroundDirective,
    UrlDirective,
    XhrDirective,
    IfLoggedDirective,
    HelloComponent,
    LoaderDirective
  ]
})
export class SharedModule { }
