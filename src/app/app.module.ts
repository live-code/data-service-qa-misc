import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { BehaviorSubject, of, Subject } from 'rxjs';
import { delay } from 'rxjs/operators';


@Injectable({ providedIn: 'root'})
export class InitService {
  isLogged$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false); // just used for ifLogged directive
  data: any;
  constructor(private http: HttpClient) {
    setTimeout(() => {
      this.isLogged$.next(true)
    }, 2000)
    setTimeout(() => {
      this.isLogged$.next(false)
    }, 3000)
    setTimeout(() => {
      this.isLogged$.next(true)
    }, 4000)
  }

  load(): Promise<any> {
    return new Promise((resolve) => {
      this.http.get<any>('https://jsonplaceholder.typicode.com/users/1')
        .pipe(delay(300))
        .subscribe(res => {
          this.data = res;
          resolve();
        });
    });
  }
}

export function onAppInit(initService: InitService): () => Promise<any> {
  return () => {
    return initService.load();
  };
}

// solution with Fetch
export let config: any;
export function onAppInit2(): () => Promise<any> {
  return () => {
    return fetch('https://jsonplaceholder.typicode.com/users/1')
      .then(res => {
        return res.json();
      })
      .then(res => {
        // console.log(res)
        config = res;
        return res;
      })
  }
}



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: onAppInit,
      deps: [InitService],
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory: onAppInit2,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
