import { Component } from '@angular/core';

@Component({
  selector: 'ds-root',
  template: `
    <button routerLink="dynamic-menu">dynamic-menu</button>
    <button routerLink="login">login</button>
    <button routerLink="home">home</button>
    
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class AppComponent {
  title = 'dataservice-demo-apps';
}
