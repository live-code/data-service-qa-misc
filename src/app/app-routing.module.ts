import { NgModule } from '@angular/core';
import { Routes, RouterModule, RouterStateSnapshot, Resolve, ActivatedRouteSnapshot, PreloadAllModules } from '@angular/router';


const routes: Routes = [
  {
    path: 'dynamic-menu',
    loadChildren: () => import('./features/dynamic-menu/dynamic-menu.module').then(m => m.DynamicMenuModule),
  },
  { path: 'home', loadChildren: () => import('./features/homepage/homepage.module').then(m => m.HomepageModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        preloadingStrategy: PreloadAllModules
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
